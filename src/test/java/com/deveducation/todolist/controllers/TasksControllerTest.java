package com.deveducation.todolist.controllers;

import com.deveducation.todolist.dto.fromuser.TaskDto;
import com.deveducation.todolist.dto.touser.TaskDtoOutgoing;
import com.deveducation.todolist.enums.TaskStatus;
import com.deveducation.todolist.security.JwtTokenProvider;
import com.deveducation.todolist.service.ITaskService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
public class TasksControllerTest {

    ObjectMapper mapper = new ObjectMapper();
    MockMvc mockMvc;

    @Mock
    JwtTokenProvider jwtTokenProvider;

    @Mock
    ITaskService taskService;

    @InjectMocks
    private static TasksController cut;

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.standaloneSetup(cut).build();
        mapper.registerModule(new JavaTimeModule());
    }

    @Test
    public void findAllTasks_valid_1() throws Exception {
        LocalDateTime deadline = LocalDateTime.now();
        Set<TaskDtoOutgoing> excepted = Set.of(
                new TaskDtoOutgoing(deadline, "task1", "task1 description", "Dmitrieva Natalia", "Dmitrieva Natalia", deadline, TaskStatus.ACCEPTED),
                new TaskDtoOutgoing(deadline, "task2", "task2 description", "Dmitrieva Natalia", "Dmitrieva Natalia", deadline, TaskStatus.IN_PROGRESS),
                new TaskDtoOutgoing(deadline, "task3", "task3 description", "Dmitrieva Natalia", "Dmitrieva Natalia", deadline, TaskStatus.ACCEPTED)
        );
        ResponseEntity<Set<TaskDtoOutgoing>> ex = new ResponseEntity<>(excepted, HttpStatus.OK);
        Mockito.when(jwtTokenProvider.validateToken("token")).thenReturn(true);
        Mockito.when(jwtTokenProvider.getLoginFromJwt("token")).thenReturn("apple");
        Mockito.when(taskService.findAllTasks("apple")).thenReturn(excepted);
        MvcResult res = mockMvc.perform(get("/tasks/all")
                .header(HttpHeaders.AUTHORIZATION, "token").accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andReturn();
        String actual = res.getResponse().getContentAsString();
        ResponseEntity<Set<TaskDtoOutgoing>> taskDtos = new ResponseEntity<>(Set.of(mapper.readValue(actual, TaskDtoOutgoing[].class)), HttpStatus.OK);
        Assert.assertEquals(ex, taskDtos);
    }

    @Test
    public void findAllTasks_valid_2() throws Exception {
        LocalDateTime deadline = LocalDateTime.now();
        Set<TaskDtoOutgoing> excepted = Set.of(
                new TaskDtoOutgoing(deadline, "task4", "task4 description", "Dmitrieva Natalia", "Marina Prikhodko", deadline, TaskStatus.ACCEPTED),
                new TaskDtoOutgoing(deadline, "task5", "task5 description", "Dmitrieva Natalia", "Dmitrieva Natalia", deadline, TaskStatus.IN_PROGRESS),
                new TaskDtoOutgoing(deadline, "task6", "task6 description", "Dmitrieva Natalia", "Serdyukov Aleksey", deadline, TaskStatus.IN_PROGRESS)
        );
        ResponseEntity<Set<TaskDtoOutgoing>> ex = new ResponseEntity<>(excepted, HttpStatus.OK);
        Mockito.when(jwtTokenProvider.validateToken("token")).thenReturn(true);
        Mockito.when(jwtTokenProvider.getLoginFromJwt("token")).thenReturn("serdyukovaleksey");
        Mockito.when(taskService.findAllTasks("serdyukovaleksey")).thenReturn(excepted);

        MvcResult res = mockMvc.perform(get("/tasks/all")
                .header(HttpHeaders.AUTHORIZATION, "token").accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andReturn();
        String actual = res.getResponse().getContentAsString();
        ResponseEntity<Set<TaskDtoOutgoing>> taskDtos = new ResponseEntity<>(Set.of(mapper.readValue(actual, TaskDtoOutgoing[].class)), HttpStatus.OK);
        Assert.assertEquals(ex, taskDtos);
    }

    @Test
    public void findAllTasks_invalid_1() throws Exception {
        Mockito.when(jwtTokenProvider.validateToken("token")).thenReturn(false);
        mockMvc.perform(get("/tasks/all")
                .header(HttpHeaders.AUTHORIZATION, "token").accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    public void findAllTasks_invalid_2() throws Exception {
        Mockito.when(jwtTokenProvider.validateToken("")).thenReturn(false);
        mockMvc.perform(get("/tasks/all")
                .header(HttpHeaders.AUTHORIZATION, "").accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    public void saveTask_valid_1() throws Exception {
        LocalDateTime deadline = LocalDateTime.now().plusDays(12);
        TaskDto taskDto = new TaskDto("task1", "description 1", deadline, "Serdyukov Aleksey", TaskStatus.ACCEPTED);
        Mockito.when(jwtTokenProvider.validateToken("token")).thenReturn(true);
        Mockito.when(jwtTokenProvider.getLoginFromJwt("token")).thenReturn("serdyukovaleksey");
        mockMvc.perform(post("/tasks/save")
                .header(HttpHeaders.AUTHORIZATION, "token").content(mapper.writeValueAsString(taskDto))
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated()).andReturn();
        Mockito.verify(taskService, Mockito.times(1)).addTask(taskDto, "serdyukovaleksey");
    }

    @Test
    public void saveTask_valid_2() throws Exception {
        LocalDateTime deadline = LocalDateTime.now().plusDays(1);
        TaskDto taskDto = new TaskDto("task2", "description dfghjmfjgh", deadline, "Dmitrieva Natalia", TaskStatus.DONE);
        Mockito.when(jwtTokenProvider.validateToken("abracadabra")).thenReturn(true);
        Mockito.when(jwtTokenProvider.getLoginFromJwt("abracadabra")).thenReturn("apple");
        mockMvc.perform(post("/tasks/save")
                .header(HttpHeaders.AUTHORIZATION, "abracadabra").content(mapper.writeValueAsString(taskDto))
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated()).andReturn();
        Mockito.verify(taskService, Mockito.times(1)).addTask(taskDto, "apple");
    }

    @Test
    public void saveTask_invalid_1() throws Exception {
        mockMvc.perform(post("/tasks/save")
                .header(HttpHeaders.AUTHORIZATION, "abracadabra").content(mapper.writeValueAsString(null))
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    public void saveTask_invalid_2() throws Exception {
        LocalDateTime deadline = LocalDateTime.now();
        TaskDto taskDto = new TaskDto("task2", "description dfghjmfjgh", deadline, "Dmitrieva Natalia", TaskStatus.DONE);
        Mockito.when(jwtTokenProvider.validateToken("abracadabra")).thenReturn(false);
        mockMvc.perform(post("/tasks/save")
                .header(HttpHeaders.AUTHORIZATION, "abracadabra").content(mapper.writeValueAsString(taskDto))
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    public void updateTask_valid_1() throws Exception {
        String token = "token";
        LocalDateTime deadline = LocalDateTime.now().plusDays(12);
        TaskDto taskDto = new TaskDto("task2", "description dfghjmfjgh", deadline, "Dmitrieva Natalia", TaskStatus.DONE);
        String taskName = "old task";
        String fullName = "Dmitrieva Natalia";
        Mockito.when(jwtTokenProvider.validateToken(token)).thenReturn(true);
        Mockito.when(jwtTokenProvider.getLoginFromJwt(token)).thenReturn("fooofoooo");
        Mockito.when(taskService.updateTask(taskDto, taskName, fullName, "fooofoooo")).thenReturn(true);
        MvcResult res = mockMvc.perform(put("/tasks/update")
                .param("taskName", taskName)
                .param("fullName", fullName)
                .header(HttpHeaders.AUTHORIZATION, token).content(mapper.writeValueAsString(taskDto))
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated()).andReturn();
        String actual = res.getResponse().getContentAsString();
        Assert.assertEquals("ok", actual);
    }

    @Test
    public void updateTask_valid_2() throws Exception {
        String token = "dfgyhy";
        LocalDateTime deadline = LocalDateTime.now().plusDays(12);
        TaskDto taskDto = new TaskDto("task2", "description dfghjmfjgh", deadline, "dghujmkhfdkuy", TaskStatus.NEW);
        String taskName = "old task";
        String fullName = "Hfdkbgd Ko";
        Mockito.when(jwtTokenProvider.validateToken(token)).thenReturn(true);
        Mockito.when(jwtTokenProvider.getLoginFromJwt(token)).thenReturn("bgbgbgbgbgbgb");
        Mockito.when(taskService.updateTask(taskDto, taskName, fullName, "bgbgbgbgbgbgb")).thenReturn(true);
        MvcResult res = mockMvc.perform(put("/tasks/update")
                .param("taskName", taskName)
                .param("fullName", fullName)
                .header(HttpHeaders.AUTHORIZATION, token).content(mapper.writeValueAsString(taskDto))
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated()).andReturn();
        String actual = res.getResponse().getContentAsString();
        Assert.assertEquals("ok", actual);
    }

    @Test
    public void updateTask_valid_3() throws Exception {
        String token = "dfgyhy";
        LocalDateTime deadline = LocalDateTime.now().plusDays(12);
        TaskDto taskDto = new TaskDto("task2", "description dfghjmfjgh", deadline, "dghujmkhfdkuy", TaskStatus.NEW);
        String taskName = "old task";
        String fullName = "Hfdkbgd Ko";
        Mockito.when(jwtTokenProvider.validateToken(token)).thenReturn(true);
        Mockito.when(jwtTokenProvider.getLoginFromJwt(token)).thenReturn("ololo90000");
        Mockito.when(taskService.updateTask(taskDto, taskName, fullName, "ololo90000")).thenReturn(false);
        MvcResult res = mockMvc.perform(put("/tasks/update")
                .param("taskName", taskName)
                .param("fullName", fullName)
                .header(HttpHeaders.AUTHORIZATION, token).content(mapper.writeValueAsString(taskDto))
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated()).andReturn();
        String actual = res.getResponse().getContentAsString();
        Assert.assertEquals("task not found", actual);
    }

    @Test
    public void updateTask_invalid_1() throws Exception {
        String token = "dfgyhy";
        LocalDateTime deadline = LocalDateTime.now();
        TaskDto taskDto = new TaskDto("task2", "description dfghjmfjgh", deadline, "dghujmkhfdkuy", TaskStatus.NEW);
        String taskName = "";
        String fullName = "xxxxxxxxxxx";
        Mockito.when(jwtTokenProvider.validateToken(token)).thenReturn(true);
        mockMvc.perform(put("/tasks/update")
                .param("taskName", taskName)
                .param("fullName", fullName)
                .header(HttpHeaders.AUTHORIZATION, token).content(mapper.writeValueAsString(taskDto))
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    public void updateTask_invalid_2() throws Exception {
        String token = "qwertyuiop";
        TaskDto taskDto = null;
        String taskName = "name name";
        mockMvc.perform(put("/tasks/update")
                .param("taskName", taskName)
                .param("fullName", "fullName")
                .header(HttpHeaders.AUTHORIZATION, token).content(mapper.writeValueAsString(taskDto))
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    public void deleteTask_valid_1() throws Exception {
        String token = "tokenidze";
        String taskName = "old task";
        String fullName = "Hfdkbgd Ko";
        Mockito.when(jwtTokenProvider.validateToken(token)).thenReturn(true);
        Mockito.when(jwtTokenProvider.getLoginFromJwt(token)).thenReturn("loginforlogin");
        Mockito.when(taskService.deleteTask(taskName, fullName, "loginforlogin")).thenReturn(true);
        MvcResult res = mockMvc.perform(delete("/tasks/delete")
                .param("taskName", taskName)
                .param("fullName", fullName)
                .header(HttpHeaders.AUTHORIZATION, token)
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    public void deleteTask_valid_2() throws Exception {
        String token = "cucumber";
        String taskName = "bad task";
        String fullName = "Kim Chi";
        Mockito.when(jwtTokenProvider.validateToken(token)).thenReturn(true);
        Mockito.when(jwtTokenProvider.getLoginFromJwt(token)).thenReturn("somethinggood");
        Mockito.when(taskService.deleteTask(taskName, fullName, "somethinggood")).thenReturn(true);
        MvcResult res = mockMvc.perform(delete("/tasks/delete")
                .param("taskName", taskName)
                .param("fullName", fullName)
                .header(HttpHeaders.AUTHORIZATION, token)
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    public void deleteTask_valid_3() throws Exception {
        String token = "cucumber";
        String taskName = "bad task";
        String fullName = "Kim Chi";
        Mockito.when(jwtTokenProvider.validateToken(token)).thenReturn(true);
        Mockito.when(jwtTokenProvider.getLoginFromJwt(token)).thenReturn("basejavacourse");
        Mockito.when(taskService.deleteTask(taskName, fullName, "basejavacourse")).thenReturn(false);
        MvcResult res = mockMvc.perform(delete("/tasks/delete")
                .param("taskName", taskName)
                .param("fullName", fullName)
                .header(HttpHeaders.AUTHORIZATION, token)
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    public void deleteTask_invalid_1() throws Exception {
        String token = "cucumber";
        String taskName = "bad task";
        String fullName = "";
        Mockito.when(jwtTokenProvider.validateToken(token)).thenReturn(true);
        mockMvc.perform(delete("/tasks/delete")
                .param("taskName", taskName)
                .param("fullName", fullName)
                .header(HttpHeaders.AUTHORIZATION, token)
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    public void deleteTask_invalid_2() throws Exception {
        String token = "cucumber";
        String taskName = "";
        String fullName = "";
        Mockito.when(jwtTokenProvider.validateToken(token)).thenReturn(true);
        mockMvc.perform(delete("/tasks/delete")
                .param("taskName", taskName)
                .param("fullName", fullName)
                .header(HttpHeaders.AUTHORIZATION, token)
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest()).andReturn();
    }
}
