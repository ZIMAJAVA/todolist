package com.deveducation.todolist.controllers;

import com.deveducation.todolist.dto.fromuser.UserAuthDto;
import com.deveducation.todolist.dto.fromuser.UserRegDto;
import com.deveducation.todolist.dto.touser.AuthResponse;
import com.deveducation.todolist.entity.User;
import com.deveducation.todolist.enums.Role;
import com.deveducation.todolist.security.JwtTokenProvider;
import com.deveducation.todolist.service.IUserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
public class UsersControllerTest {
    ObjectMapper mapper = new ObjectMapper();
    MockMvc mockMvc;

    @InjectMocks
    private UsersController cut;

    @Mock
    private IUserService usersService;

    @Mock
    private JwtTokenProvider jwtTokenProvider;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(cut).build();
    }

    @Test
    public void submitReg_validData_1() throws Exception {
        UserRegDto userRegDto = new UserRegDto("apple111", "Dmitrieva Natalia", "password",
                "nataliadmitrieva155@gmail.com", "+380988220688");

        mockMvc.perform(post("/user/reg")
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE).content(mapper.writeValueAsString(userRegDto)))
                .andExpect(status().isCreated()).andReturn();

        Mockito.verify(usersService, Mockito.times(1)).addNewUser(userRegDto);
    }

    @Test
    public void submitReg_validData_2() throws Exception {
        UserRegDto userRegDto = new UserRegDto("cucumber", "Ko Che Lya", "000password333",
                "yanata888@gmail.com", "+38090000000");

        mockMvc.perform(post("/user/reg")
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE).content(mapper.writeValueAsString(userRegDto)))
                .andExpect(status().isCreated()).andReturn();

        Mockito.verify(usersService, Mockito.times(1)).addNewUser(userRegDto);
    }

    @Test
    public void submitReg_invalidData_userIsNull() throws Exception {
        UserRegDto userRegDto = null;

        mockMvc.perform(post("/user/reg")
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE).content(mapper.writeValueAsString(userRegDto)))
                .andExpect(status().isBadRequest()).andReturn();

        Mockito.verify(usersService, Mockito.times(0)).addNewUser(userRegDto);
    }

    @Test
    public void submitAuth_validData() throws Exception {
        UserAuthDto userAuthDto = new UserAuthDto("ffffffffffff", "llllllll44444");
        User user = new User("ffffffffffff", "fullName", "llllllll44444",
                "yanata888@gmail.com", "+38090000000", Role.ADMIN);
        Mockito.when(usersService.findUserByLoginAndPassword(userAuthDto.getLogin(), userAuthDto.getPassword())).thenReturn(user);
        Mockito.when(jwtTokenProvider.accessToken(user)).thenReturn("welcome to hell");

        MvcResult res = mockMvc.perform(post("/user/auth")
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(userAuthDto)))
                .andExpect(status().isOk()).andReturn();

        String actual = res.getResponse().getContentAsString();
        Assert.assertEquals(new AuthResponse("welcome to hell"), mapper.readValue(actual, AuthResponse.class));
    }

    @Test
    public void submitAuth_invalidLogin() throws Exception {
        UserAuthDto userAuthDto = new UserAuthDto("", "llllllll44444");
        User user = new User("ffffffffffff", "fullName", "llllllll44444",
                "yanata888@gmail.com", "+38090000000", Role.ADMIN);
        Mockito.when(usersService.findUserByLoginAndPassword(userAuthDto.getLogin(), userAuthDto.getPassword())).thenReturn(user);
        Mockito.when(jwtTokenProvider.accessToken(user)).thenReturn("welcome to hell");

        mockMvc.perform(post("/user/auth")
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(userAuthDto)))
                .andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    public void submitAuth_invalidData_userAuthDtoNull() throws Exception {
        UserAuthDto userAuthDto = null;
        User user = new User("ffffffffffff", "fullName", "llllllll44444",
                "yanata888@gmail.com", "+38090000000", Role.ADMIN);

        Mockito.when(jwtTokenProvider.accessToken(user)).thenReturn("welcome to hell");

        mockMvc.perform(post("/user/auth")
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(userAuthDto)))
                .andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    public void submitAuth_invalidData_userNull() throws Exception {
        UserAuthDto userAuthDto = new UserAuthDto("", "llllllll44444");
        User user = null;

        Mockito.when(usersService.findUserByLoginAndPassword(userAuthDto.getLogin(), userAuthDto.getPassword())).thenReturn(user);

        mockMvc.perform(post("/user/auth")
                .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(userAuthDto)))
                .andExpect(status().isBadRequest()).andReturn();
    }

}