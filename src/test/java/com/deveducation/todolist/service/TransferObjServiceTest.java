package com.deveducation.todolist.service;

import com.deveducation.todolist.dto.touser.TaskDtoOutgoing;
import com.deveducation.todolist.entity.Task;
import com.deveducation.todolist.entity.User;
import com.deveducation.todolist.enums.Role;
import com.deveducation.todolist.enums.TaskStatus;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class TransferObjServiceTest {

    private ITransferObjService cut = new TransferObjService();

    @Test
    public void toTaskDtoOutgoingList1() {
        User userAdmin = new User("apple", "Dmitrieva Natalia", "", "", "", Role.ADMIN);
        User userUser = new User("marinaprikhodko", "Marina Prikhodko", "", "", "", Role.USER);
        LocalDateTime dateTime = LocalDateTime.of(2020, 10, 5, 12, 0);
        List<Task> tasks = Arrays.asList(
                new Task(dateTime, "task1", "task1 description", userUser, userUser, dateTime, TaskStatus.NEW),
                new Task(dateTime, "task2", "task2 description", userAdmin, userUser, dateTime, TaskStatus.ACCEPTED),
                new Task(dateTime, "task3", "task3 description", userUser, userAdmin, dateTime, TaskStatus.DECLINED),
                new Task(dateTime, "task4", "task4 description", userAdmin, userAdmin, dateTime, TaskStatus.NEW)
        );
        Set<TaskDtoOutgoing> taskDtoOutgoingList = Set.of(
                new TaskDtoOutgoing(dateTime, "task1", "task1 description", "Marina Prikhodko", "Marina Prikhodko", dateTime, TaskStatus.NEW),
                new TaskDtoOutgoing(dateTime, "task2", "task2 description", "Dmitrieva Natalia", "Marina Prikhodko", dateTime, TaskStatus.ACCEPTED),
                new TaskDtoOutgoing(dateTime, "task3", "task3 description", "Marina Prikhodko", "Dmitrieva Natalia", dateTime, TaskStatus.DECLINED),
                new TaskDtoOutgoing(dateTime, "task4", "task4 description", "Dmitrieva Natalia", "Dmitrieva Natalia", dateTime, TaskStatus.NEW)
        );
        Set<TaskDtoOutgoing> act = cut.toTaskDOutgoingList(tasks);
        assertEquals(act, taskDtoOutgoingList);
    }

    @Test
    public void toTaskDtoOutgoingList2() {
        User userUser = new User("marinaprikhodko", "Marina Prikhodko", "", "", "", Role.USER);
        LocalDateTime dateTime = LocalDateTime.of(2020, 10, 5, 12, 0);
        List<Task> tasks = Arrays.asList(
                new Task(dateTime, "task1", "task1 description", userUser, userUser, dateTime, TaskStatus.NEW),
                null, null);
        Set<TaskDtoOutgoing> taskDtoOutgoingList = Set.of(
                new TaskDtoOutgoing(dateTime, "task1", "task1 description", "Marina Prikhodko", "Marina Prikhodko", dateTime, TaskStatus.NEW));
        Set<TaskDtoOutgoing> act = cut.toTaskDOutgoingList(tasks);
        assertEquals(act, taskDtoOutgoingList);
    }

}