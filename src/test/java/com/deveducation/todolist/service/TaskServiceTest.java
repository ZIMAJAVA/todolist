package com.deveducation.todolist.service;

import com.deveducation.todolist.dto.touser.TaskDtoOutgoing;
import com.deveducation.todolist.entity.Task;
import com.deveducation.todolist.entity.User;
import com.deveducation.todolist.enums.Role;
import com.deveducation.todolist.enums.TaskStatus;
import com.deveducation.todolist.exception.UserNotFoundExeption;
import com.deveducation.todolist.repository.ITaskRepo;
import com.deveducation.todolist.repository.IUserRepo;
import org.junit.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.*;

import static com.deveducation.todolist.enums.TaskStatus.DECLINED;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class TaskServiceTest {

    private ITaskRepo taskRepo = Mockito.mock(ITaskRepo.class);
    private IUserRepo userRepo = Mockito.mock(IUserRepo.class);
    private TransferObjService transferObjService = Mockito.mock(TransferObjService.class);
    private ITaskService cut = new TaskService(taskRepo, userRepo, transferObjService);

    @Test
    public void findAllTasksAdminUser() {
        String login = "apple";
        Optional<User> userAdmin = Optional.of(new User("apple", "Dmitrieva Natalia", "", "", "", Role.ADMIN));
        User userUser = new User("marinaprikhodko", "Marina Prikhodko", "", "", "", Role.USER);
        LocalDateTime dateTime = LocalDateTime.of(2020, 10, 5, 12, 0);
        List<Task> tasks = Arrays.asList(
                new Task(dateTime, "task1", "task1 description", userUser, userUser, dateTime, TaskStatus.NEW),
                new Task(dateTime, "task2", "task2 description", userAdmin.get(), userUser, dateTime, TaskStatus.ACCEPTED),
                new Task(dateTime, "task3", "task3 description", userUser, userAdmin.get(), dateTime, TaskStatus.DECLINED),
                new Task(dateTime, "task4", "task4 description", userAdmin.get(), userAdmin.get(), dateTime, TaskStatus.NEW)
        );
        Set<TaskDtoOutgoing> taskDtoOutgoingList = Set.of(
                new TaskDtoOutgoing(dateTime, "task1", "task1 description", "Marina Prikhodko", "Marina Prikhodko", dateTime, TaskStatus.NEW),
                new TaskDtoOutgoing(dateTime, "task2", "task2 description", "Dmitrieva Natalia", "Marina Prikhodko", dateTime, TaskStatus.ACCEPTED),
                new TaskDtoOutgoing(dateTime, "task3", "task3 description", "Marina Prikhodko", "Dmitrieva Natalia", dateTime, TaskStatus.DECLINED),
                new TaskDtoOutgoing(dateTime, "task4", "task4 description", "Dmitrieva Natalia", "Dmitrieva Natalia", dateTime, TaskStatus.NEW)
        );
        Mockito.when(userRepo.findByLoginIgnoreCase(login)).thenReturn(userAdmin);
        Mockito.when(taskRepo.findAll()).thenReturn(tasks);
        Mockito.when(transferObjService.toTaskDOutgoingList(tasks)).thenReturn(taskDtoOutgoingList);
        Set<TaskDtoOutgoing> act = cut.findAllTasks(login);
        assertEquals(act, taskDtoOutgoingList);
    }

    @Test
    public void findAllTasksRegularUser() {
        String login = "marinaprikhodko";
        User userAdmin = new User("apple", "Dmitrieva Natalia", "", "", "", Role.ADMIN);
        Optional<User> userUser = Optional.of(new User("marinaprikhodko", "Marina Prikhodko", "", "", "", Role.USER));
        LocalDateTime dateTime = LocalDateTime.of(2020, 10, 5, 12, 0);
        List<Task> tasks = Arrays.asList(
                new Task(dateTime, "task1", "task1 description", userUser.get(), userUser.get(), dateTime, TaskStatus.NEW),
                new Task(dateTime, "task2", "task2 description", userAdmin, userUser.get(), dateTime, TaskStatus.ACCEPTED)
        );
        Set<TaskDtoOutgoing> taskDtoOutgoingList = Set.of(
                new TaskDtoOutgoing(dateTime, "task1", "task1 description", "Marina Prikhodko", "Marina Prikhodko", dateTime, TaskStatus.NEW),
                new TaskDtoOutgoing(dateTime, "task2", "task2 description", "Dmitrieva Natalia", "Marina Prikhodko", dateTime, TaskStatus.ACCEPTED)
        );
        Mockito.when(userRepo.findByLoginIgnoreCase(login)).thenReturn(userUser);
        Mockito.when(taskRepo
                .findByAuthorAndTaskStatusIsNotOrExecutorAndTaskStatusIsNot(userUser.get(), DECLINED, userUser.get(), TaskStatus.DECLINED)).thenReturn(tasks);
        Mockito.when(transferObjService.toTaskDOutgoingList(tasks)).thenReturn(taskDtoOutgoingList);

        Set<TaskDtoOutgoing> act = cut.findAllTasks(login);
        assertEquals(act, taskDtoOutgoingList);

    }

    @Test
    public void findAllTasksUserIsNull() {
        String login = "";
        Mockito.when(userRepo.findByLoginIgnoreCase(login)).thenReturn(Optional.empty());

        assertThrows(UserNotFoundExeption.class, () -> cut.findAllTasks(login));
    }
}