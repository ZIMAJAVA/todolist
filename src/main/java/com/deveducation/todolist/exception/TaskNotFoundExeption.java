package com.deveducation.todolist.exception;

public class TaskNotFoundExeption extends RuntimeException {

    public TaskNotFoundExeption(String taskName, String author) {

        super(String.format("Task named %s with author %s not found", taskName, author));

    }
}
