package com.deveducation.todolist.exception;

public class DtoConvertExeption extends RuntimeException {

    public DtoConvertExeption() {
        super("Error during convert DTO to Entity");
    }
}
