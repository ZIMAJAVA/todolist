package com.deveducation.todolist.exception;

public class TaskAlreadyExistsExeption extends RuntimeException {

    public TaskAlreadyExistsExeption(String message) {

        super(message);
    }
}
