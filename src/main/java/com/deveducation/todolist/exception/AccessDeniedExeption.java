package com.deveducation.todolist.exception;

public class AccessDeniedExeption extends RuntimeException {

    public AccessDeniedExeption(String message) {

        super(message);
    }
}
