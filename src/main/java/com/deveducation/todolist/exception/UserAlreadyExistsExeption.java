package com.deveducation.todolist.exception;

public class UserAlreadyExistsExeption extends RuntimeException {

    public UserAlreadyExistsExeption(String message) {

        super(message);
    }
}
