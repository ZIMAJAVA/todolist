package com.deveducation.todolist.dto;

public class RegexpConstants {
    public static final String TASKS_NAME = "^[^\\s][?!,.a-zA-Z0-9\\s]*$";
    public static final String TASK_DESCRIPTION = "^[^\\s][?!,.a-zA-Z0-9\\s]*$";
    public static final String TASK_DEADLINE = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String USER_FULL_NAME = "^[a-zA-Z]{2,}(?: [a-zA-Z]+){0,2}$";
    public static final String USER_LOGIN = "^[a-zA-Z][a-zA-Z0-9]{5,29}";
    public static final String USER_PASSWORD = "[a-zA-Z0-9]{6,30}";
    public static final String EMAIL = "^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$";
    public static final String PHONE = "^[+][0-9]{10,15}";

}
