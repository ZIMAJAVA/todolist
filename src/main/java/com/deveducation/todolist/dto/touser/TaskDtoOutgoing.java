package com.deveducation.todolist.dto.touser;

import com.deveducation.todolist.enums.TaskStatus;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.time.LocalDateTime;

@Value
public class TaskDtoOutgoing {
    LocalDateTime timestamp;
    String name;
    String description;
    String author;
    String executor;
    LocalDateTime deadline;
    TaskStatus taskStatus;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public TaskDtoOutgoing(@JsonProperty("timestamp") LocalDateTime timestamp,
                           @JsonProperty("name") String name,
                           @JsonProperty("description") String description,
                           @JsonProperty("author") String author,
                           @JsonProperty("executor") String executor,
                           @JsonProperty("deadline") LocalDateTime deadline,
                           @JsonProperty("taskStatus") TaskStatus taskStatus) {
        this.timestamp = timestamp;
        this.name = name;
        this.description = description;
        this.author = author;
        this.executor = executor;
        this.deadline = deadline;
        this.taskStatus = taskStatus;
    }
}
