package com.deveducation.todolist.dto.fromuser;

import com.deveducation.todolist.dto.RegexpConstants;
import com.deveducation.todolist.enums.TaskStatus;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.time.LocalDateTime;

@Value
public class TaskDto {

    @Size(min = 1, max = 255, message = "Length of name must be in a range from 1 to 30")
    @NotNull(message = "Please provide name")
    @Pattern(regexp = RegexpConstants.TASKS_NAME, message = "Name must contain only Latin letters and digits 0 to 9.")
    String name;

    @Size(max = 255, message = "Length of description must be in a range from 1 to 255")
    @Pattern(regexp = RegexpConstants.TASK_DESCRIPTION, message = "Description must contain only Latin letters and digits 0 to 9.")
    String description;

    @DateTimeFormat(pattern = RegexpConstants.TASK_DEADLINE)
    @FutureOrPresent(message = "Please provide current or future date")
    LocalDateTime deadline;

    @Size(min = 2, max = 50, message = "Length of executor must be in a range from 2 to 50")
    @NotNull(message = "Executor must not be null")
    @Pattern(regexp = RegexpConstants.USER_FULL_NAME, message = "Executor must contain only latin letters, max 3 words and 2 spaces")
    String executor;

    @NotNull(message = "Status must be not null")
    TaskStatus taskStatus;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public TaskDto(@JsonProperty("name") String name,
                   @JsonProperty("description") String description,
                   @JsonProperty("deadline") LocalDateTime deadline,
                   @JsonProperty("executor") String executor,
                   @JsonProperty("taskStatus") TaskStatus taskStatus) {
        this.name = name;
        this.description = description;
        this.deadline = deadline;
        this.executor = executor;
        this.taskStatus = taskStatus;
    }
}

