package com.deveducation.todolist.dto.fromuser;

import com.deveducation.todolist.dto.RegexpConstants;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import javax.validation.constraints.*;

@Value
public class UserRegDto {

    @Size(min = 6, max = 30, message = "Length of login must be in a range from 6 to 30")
    @NotNull(message = "Login must not be null")
    @Pattern(regexp = RegexpConstants.USER_LOGIN, message = "Login must contain only Latin letters and digits 0 to 9.")
    String login;

    @Size(min = 2, max = 50, message = "Length of full name must be in a range from 2 to 50")
    @NotNull(message = "Full name must not be null")
    @Pattern(regexp = RegexpConstants.USER_FULL_NAME, message = "Full name must contain only latin letters, max 3 words and 2 spaces")
    String fullName;

    @Size(min = 6, max = 30, message = "Length of password must be in a range from 6 to 30")
    @NotNull(message = "Password must not be null")
    @Pattern(regexp = RegexpConstants.USER_PASSWORD, message = "Password must contain only Latin letters and digits 0 to 9")
    String password;

    @Email(message = "Not valid mail format")
    @Pattern(regexp = RegexpConstants.EMAIL, message = "Not valid mail format")
    String email;

    @Size(min = 11, max = 15, message = "Length of phone must be in a range from 10 to 15")
    @Pattern(regexp = RegexpConstants.PHONE, message = "The phone number must be in format +XXXXXXXXXX")
    String phone;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public UserRegDto(@JsonProperty("login") String login,
                      @JsonProperty("fullName") String fullName,
                      @JsonProperty("password") String password,
                      @JsonProperty("email") String email,
                      @JsonProperty("phone") String phone) {
        this.login = login;
        this.fullName = fullName;
        this.password = password;
        this.email = email;
        this.phone = phone;
    }
}

