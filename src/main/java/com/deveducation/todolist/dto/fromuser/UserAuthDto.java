package com.deveducation.todolist.dto.fromuser;

import com.deveducation.todolist.dto.RegexpConstants;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Value
public class UserAuthDto {

    @Size(min = 6, max = 30, message = "Length of login must be in a range from 6 to 30")
    @NotNull(message = "Login must not be null")
    @Pattern(regexp = RegexpConstants.USER_LOGIN, message = "Login must contain only Latin letters and digits 0 to 9.")
    String login;

    @Size(min = 6, max = 30, message = "Length of password must be in a range from 6 to 30")
    @NotNull(message = "Password must not be null")
    @Pattern(regexp = RegexpConstants.USER_PASSWORD, message = "Password must contain only Latin letters and digits 0 to 9.")
    String password;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public UserAuthDto(@JsonProperty("login") String login,
                       @JsonProperty("password") String password) {
        this.login = login;
        this.password = password;
    }
}
