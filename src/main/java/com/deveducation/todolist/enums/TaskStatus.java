package com.deveducation.todolist.enums;


public enum TaskStatus {
    NEW, ACCEPTED, IN_PROGRESS, DONE, DECLINED;
}
