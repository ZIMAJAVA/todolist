package com.deveducation.todolist.enums;

public enum Role {
    USER, ADMIN;
}
