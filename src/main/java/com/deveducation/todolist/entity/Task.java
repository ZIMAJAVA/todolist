package com.deveducation.todolist.entity;

import com.deveducation.todolist.enums.TaskStatus;
import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.LocalDateTime;


@Data
@Entity
@NoArgsConstructor
@Table(name = "tasks", uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "author_id"})})
@TypeDef(name = "pgsql_enum", typeClass = PostgreSQLEnumType.class)
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BaseGenerator")
    @SequenceGenerator(name = "BaseGenerator", sequenceName = "hibernate_sequence", allocationSize = 1)
    private Long id;

    @Column(name = "timestamp", nullable = false)
    private LocalDateTime timestamp;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id", referencedColumnName = "id", nullable = false)
    private User author;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JoinColumn(name = "executor_id", referencedColumnName = "id", nullable = false)
    private User executor;

    @Column(name = "deadline")
    private LocalDateTime deadline;

    @Column(name = "status", nullable = false)
    @Type(type = "pgsql_enum")
    @Enumerated(EnumType.STRING)
    private TaskStatus taskStatus;

    public Task(LocalDateTime timestamp, String name, String description,
                User author, User executor, LocalDateTime deadline, TaskStatus taskStatus) {
        this.timestamp = timestamp;
        this.name = name;
        this.description = description;
        this.author = author;
        this.executor = executor;
        this.deadline = deadline;
        this.taskStatus = taskStatus;
    }
}
