package com.deveducation.todolist.service;


import com.deveducation.todolist.dto.fromuser.TaskDto;
import com.deveducation.todolist.dto.touser.TaskDtoOutgoing;
import com.deveducation.todolist.exception.UserNotFoundExeption;

import java.util.Set;

public interface ITaskService {
    void addTask(TaskDto taskDto, String login);

    Set<TaskDtoOutgoing> findAllTasks(String login) throws UserNotFoundExeption;

    boolean updateTask(TaskDto taskDto, String taskName, String authorFullName, String login);

    boolean deleteTask(String name, String authorFullName, String login);
}
