package com.deveducation.todolist.service;

import com.deveducation.todolist.dto.fromuser.TaskDto;
import com.deveducation.todolist.dto.fromuser.UserRegDto;
import com.deveducation.todolist.dto.touser.TaskDtoOutgoing;
import com.deveducation.todolist.entity.Task;
import com.deveducation.todolist.entity.User;
import com.deveducation.todolist.enums.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


@Service
public class TransferObjService implements ITransferObjService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Set<TaskDtoOutgoing> toTaskDOutgoingList(List<Task> taskList) {
        return taskList
                .stream()
                .filter(Objects::nonNull)
                .map(t -> toTaskDtoOutgoing(t).get())
                .collect(Collectors.toSet());
    }

    @Override
    public Optional<TaskDtoOutgoing> toTaskDtoOutgoing(Task t) {
        return t != null ?
                Optional.of(new TaskDtoOutgoing(t.getTimestamp(), t.getName(), t.getDescription(), t.getAuthor().getFullName(),
                        t.getExecutor().getFullName(), t.getDeadline(), t.getTaskStatus())) :
                Optional.empty();
    }

    @Override
    public Optional<Task> toTask(TaskDto taskDto, User author, User executor) {
        return taskDto != null ?
                Optional.of(new Task(null, taskDto.getName(), taskDto.getDescription(),
                        author, executor, taskDto.getDeadline(), null)) :
                Optional.empty();
    }

    @Override
    public Optional<User> toUser(UserRegDto userRegDto) {
        return userRegDto != null ?
                Optional.of(new User(userRegDto.getLogin(), userRegDto.getFullName(), passwordEncoder.encode(userRegDto.getPassword()),
                        userRegDto.getEmail(), userRegDto.getPhone(), Role.USER)) :
                Optional.empty();
    }

}
