package com.deveducation.todolist.service;

import com.deveducation.todolist.dto.fromuser.UserRegDto;
import com.deveducation.todolist.entity.User;
import com.deveducation.todolist.exception.AccessDeniedExeption;
import com.deveducation.todolist.exception.DtoConvertExeption;
import com.deveducation.todolist.exception.UserAlreadyExistsExeption;
import com.deveducation.todolist.exception.UserNotFoundExeption;
import com.deveducation.todolist.repository.IUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService implements IUserService {

    @Autowired
    private IUserRepo userRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TransferObjService transferObjService;

    public User findByLogin(String login) {
        return getUserByLoginElseThrow(login);
    }

    @Override
    public void addNewUser(UserRegDto userRegDto) {
        String login = userRegDto.getLogin();
        String fullName = userRegDto.getFullName();
        if (userRepo.existsByLoginIgnoreCase(login)) {
            throw new UserAlreadyExistsExeption(String.format("User with login %s already exists", login));
        }
        if (userRepo.existsByFullNameIgnoreCase(fullName)) {
            throw new UserAlreadyExistsExeption(String.format("User with full name %s already exists", fullName));
        }
        Optional<User> user = transferObjService.toUser(userRegDto);
        if (user.isPresent()) {
            userRepo.save(user.get());
        } else {
            throw new DtoConvertExeption();
        }

    }

    @Override
    public User findUserByLoginAndPassword(String login, String password) {
        User user = getUserByLoginElseThrow(login);
        if (user != null && passwordEncoder.matches(password, user.getPassword())) {
            return user;
        } else {
            throw new AccessDeniedExeption("Wrong password");
        }

    }

    public User getUserByLoginElseThrow(String login) {
        Optional<User> user = userRepo.findByLoginIgnoreCase(login);
        if (user.isEmpty()) {
            throw new UserNotFoundExeption(String.format("User with login %s not found", login));
        } else {
            return user.get();
        }
    }
}
