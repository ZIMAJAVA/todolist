package com.deveducation.todolist.service;

import com.deveducation.todolist.dto.fromuser.UserRegDto;
import com.deveducation.todolist.entity.User;

public interface IUserService {
    void addNewUser(UserRegDto userRegDto);
    User findUserByLoginAndPassword(String login, String password);
}
