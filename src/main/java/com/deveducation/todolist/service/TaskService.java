package com.deveducation.todolist.service;

import com.deveducation.todolist.dto.fromuser.TaskDto;
import com.deveducation.todolist.dto.touser.TaskDtoOutgoing;
import com.deveducation.todolist.entity.Task;
import com.deveducation.todolist.entity.User;
import com.deveducation.todolist.enums.TaskStatus;
import com.deveducation.todolist.exception.AccessDeniedExeption;
import com.deveducation.todolist.exception.TaskAlreadyExistsExeption;
import com.deveducation.todolist.exception.TaskNotFoundExeption;
import com.deveducation.todolist.exception.UserNotFoundExeption;
import com.deveducation.todolist.repository.ITaskRepo;
import com.deveducation.todolist.repository.IUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.deveducation.todolist.enums.Role.ADMIN;
import static com.deveducation.todolist.enums.TaskStatus.DECLINED;


@Service
public class TaskService implements ITaskService {

    private final ITaskRepo taskRepo;
    private final IUserRepo userRepo;
    private final ITransferObjService transferObjService;

    @Autowired
    public TaskService(ITaskRepo taskRepo, IUserRepo userRepo, TransferObjService transferObjService) {
        this.taskRepo = taskRepo;
        this.userRepo = userRepo;
        this.transferObjService = transferObjService;
    }

    @Override
    public void addTask(TaskDto taskDto, String login) {
        User user = getUserByLoginElseThrow(login);
        if (taskRepo.existsByNameIgnoreCaseAndAuthor(taskDto.getName(), user)) {
            throw new TaskAlreadyExistsExeption(String.format("Task named %s with author %s already exists", taskDto.getName(), user.getFullName()));
        }

        Task task = new Task(LocalDateTime.now(), taskDto.getName(), taskDto.getDescription(),
                user, getUserByFullNameElseThrow(taskDto.getExecutor()),
                taskDto.getDeadline(), TaskStatus.NEW);

        taskRepo.save(task);
    }

    @Override
    @Transactional(readOnly = true)
    public Set<TaskDtoOutgoing> findAllTasks(String login) throws UserNotFoundExeption {
        Optional<User> user = userRepo.findByLoginIgnoreCase(login);
        if (user.isEmpty()) {
            throw new UserNotFoundExeption(login);
        }
        List<Task> tasks = user.get().getRole().equals(ADMIN) ?
                taskRepo.findAll() :
                taskRepo.findByAuthorAndTaskStatusIsNotOrExecutorAndTaskStatusIsNot(user.get(), DECLINED, user.get(), DECLINED);

        return transferObjService.toTaskDOutgoingList(tasks);
    }

    @Override
    public boolean updateTask(TaskDto taskDto, String taskName, String authorFullName, String login) {
        User author = getUserByFullNameElseThrow(authorFullName);
        Optional<Task> task = taskRepo.findTaskByNameIgnoreCaseAndAuthor(taskName, author);
        if (task.isEmpty()) {
            throw new TaskNotFoundExeption(taskName, authorFullName);
        }
        User currentUser = getUserByLoginElseThrow(login);
        if (!checkIsAccessGranted(task.get(), currentUser)) {
            throw new AccessDeniedExeption("You haven't permissions to access this task");
        }
        User executor = getUserByFullNameElseThrow(taskDto.getExecutor());

        task.ifPresent(t -> {
            t.setDescription(taskDto.getDescription());
            t.setDeadline(taskDto.getDeadline());
            t.setExecutor(executor);
            t.setTaskStatus(taskDto.getTaskStatus());
            taskRepo.save(t);
        });
        return true;
    }

    @Override
    public boolean deleteTask(String taskName, String authorFullName, String login) {
        User author = getUserByFullNameElseThrow(authorFullName);
        Optional<Task> task = taskRepo.findTaskByNameIgnoreCaseAndAuthor(taskName, author);
        if (task.isEmpty() || task.get().getTaskStatus() == DECLINED) {
            throw new TaskNotFoundExeption(taskName, authorFullName);
        }
        User currentUser = getUserByLoginElseThrow(login);
        if (!checkIsAccessGranted(task.get(), currentUser)) {
            throw new AccessDeniedExeption("You haven't permissions to access this task");
        }

        task.ifPresent(t -> {
            t.setTaskStatus(DECLINED);
            taskRepo.save(t);
        });
        return true;

    }

    private User getUserByLoginElseThrow(String login) {
        Optional<User> user = userRepo.findByLoginIgnoreCase(login);
        if (user.isEmpty()) {
            throw new UserNotFoundExeption(String.format("User with login %s not found", login));
        } else {
            return user.get();
        }
    }

    private User getUserByFullNameElseThrow(String fullName) {
        Optional<User> user = userRepo.findByFullNameIgnoreCase(fullName);
        if (user.isEmpty()) {
            throw new UserNotFoundExeption(String.format("User with full name %s not found", fullName));
        } else {
            return user.get();
        }
    }

    private boolean checkIsAccessGranted(Task task, User user) {
        return user.getRole().equals(ADMIN) || task.getAuthor() == user || task.getExecutor() == user;
    }
}
