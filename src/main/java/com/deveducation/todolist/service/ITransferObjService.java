package com.deveducation.todolist.service;

import com.deveducation.todolist.dto.fromuser.TaskDto;
import com.deveducation.todolist.dto.fromuser.UserRegDto;
import com.deveducation.todolist.dto.touser.TaskDtoOutgoing;
import com.deveducation.todolist.entity.Task;
import com.deveducation.todolist.entity.User;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ITransferObjService {
    Set<TaskDtoOutgoing> toTaskDOutgoingList(List<Task> taskList);

    Optional <TaskDtoOutgoing> toTaskDtoOutgoing(Task task);

    Optional<Task> toTask (TaskDto taskDto, User author, User executor);

    Optional<User> toUser(UserRegDto userRegDto);
}
