package com.deveducation.todolist.controllers;

import com.deveducation.todolist.dto.fromuser.UserAuthDto;
import com.deveducation.todolist.dto.fromuser.UserRegDto;
import com.deveducation.todolist.dto.touser.AuthResponse;
import com.deveducation.todolist.entity.User;
import com.deveducation.todolist.security.JwtTokenProvider;
import com.deveducation.todolist.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
public class UsersController {

    @Autowired
    private IUserService usersService;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @PostMapping(path = "/reg")
    ResponseEntity<Void> submitReg(@Valid @RequestBody UserRegDto userRegDto) {
        if (userRegDto != null) {
            usersService.addNewUser(userRegDto);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @PostMapping(path = "/auth")
    ResponseEntity<AuthResponse> submitAuth(@Valid @RequestBody UserAuthDto userAuthDto) {
        if (userAuthDto != null) {
            User user = usersService.findUserByLoginAndPassword(userAuthDto.getLogin(), userAuthDto.getPassword());
            if (user != null) {
                String token = jwtTokenProvider.accessToken(user);
                return ResponseEntity.status(HttpStatus.OK).body(new AuthResponse(token));
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
}