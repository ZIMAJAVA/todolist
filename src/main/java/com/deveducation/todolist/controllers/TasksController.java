package com.deveducation.todolist.controllers;

import com.deveducation.todolist.dto.fromuser.TaskDto;
import com.deveducation.todolist.dto.touser.TaskDtoOutgoing;
import com.deveducation.todolist.security.JwtTokenProvider;
import com.deveducation.todolist.service.ITaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@RestController
@RequestMapping("/tasks")
public class TasksController {
    private static final String OK = "ok";
    private static final String FAILED = "task not found";

    @Autowired
    private ITaskService taskService;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Set<TaskDtoOutgoing>> findAllTasks(@RequestHeader(HttpHeaders.AUTHORIZATION) String token) {
        if (isValidHeader(token)) {
            String login = jwtTokenProvider.getLoginFromJwt(token);
            return new ResponseEntity<>(taskService.findAllTasks(login), HttpStatus.OK);
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping(path = "/save", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Void> saveTask(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                                  @Valid @RequestBody TaskDto taskDto) {
        if (isValidHeader(token) && taskDto != null) {
            String login = jwtTokenProvider.getLoginFromJwt(token);
            taskService.addTask(taskDto, login);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return ResponseEntity.badRequest().build();
    }

    @PutMapping(path = "/update", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<String> updateTask(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                                      @Valid @RequestBody TaskDto taskDto,
                                      @RequestParam(name = "taskName") String taskName,
                                      @RequestParam(name = "fullName") String authorFullName) {
        if (isValidHeader(token) && taskDto != null && !taskName.equals("") && !authorFullName.equals("")) {
            String login = jwtTokenProvider.getLoginFromJwt(token);
            String responseBody = taskService.updateTask(taskDto, taskName, authorFullName, login) ? OK : FAILED;
            return ResponseEntity.status(HttpStatus.CREATED).body(responseBody);
        }
        return ResponseEntity.badRequest().build();
    }

    @DeleteMapping(path = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Void> deleteTask(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                                    @RequestParam(name = "taskName") String taskName,
                                    @RequestParam(name = "fullName") String fullName) {
        if (isValidHeader(token) && !taskName.equals("") && !fullName.equals("")) {
            String login = jwtTokenProvider.getLoginFromJwt(token);
            taskService.deleteTask(taskName, fullName, login);
            return ResponseEntity.status(HttpStatus.OK).build();
        }
        return ResponseEntity.badRequest().build();
    }

    private boolean isValidHeader(String token) {
        return !token.isEmpty() && jwtTokenProvider.validateToken(token);
    }
}

