package com.deveducation.todolist.repository;

import com.deveducation.todolist.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IUserRepo extends JpaRepository<User, Long> {
    Optional<User> findByLoginIgnoreCase(String login);

    Optional<User> findByFullNameIgnoreCase(String fullName);

    boolean existsByLoginIgnoreCase(String login);

    boolean existsByFullNameIgnoreCase(String fullName);
}
