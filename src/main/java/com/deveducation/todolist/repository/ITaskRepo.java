package com.deveducation.todolist.repository;

import com.deveducation.todolist.entity.Task;
import com.deveducation.todolist.entity.User;
import com.deveducation.todolist.enums.TaskStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ITaskRepo extends JpaRepository<Task, Long> {
    List<Task> findByAuthorAndTaskStatusIsNotOrExecutorAndTaskStatusIsNot(User author, TaskStatus taskStatusAuthor, User exec, TaskStatus taskStatusExec);

    Optional<Task> findTaskByNameIgnoreCaseAndAuthor(String name, User author);

    boolean existsByNameIgnoreCaseAndAuthor(String name, User author);
}
