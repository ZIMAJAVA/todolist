-- ----------------------------
-- Type structure for status
-- ----------------------------
DROP TYPE IF EXISTS status;
CREATE TYPE status AS ENUM (
    'NEW',
    'ACCEPTED',
    'IN_PROGRESS',
    'DONE',
    'DECLINED'
    );

-- ----------------------------
-- Type structure for role
-- ----------------------------
DROP TYPE IF EXISTS role;
CREATE TYPE role AS ENUM (
    'USER',
    'ADMIN'
    );

-- ----------------------------
-- Sequence structure for hibernate_sequence
-- ----------------------------
DROP SEQUENCE IF EXISTS hibernate_sequence;
CREATE SEQUENCE hibernate_sequence
    INCREMENT 1
    MINVALUE  1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Table structure for tasks
-- ----------------------------
DROP TABLE IF EXISTS tasks;
CREATE TABLE tasks (
                       id int8 NOT NULL,
                       deadline timestamp(6),
                       description varchar(255),
                       name varchar(255) NOT NULL,
                       status status NOT NULL,
                       timestamp timestamp(6) NOT NULL,
                       author_id int8 NOT NULL,
                       executor_id int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS users;
CREATE TABLE users (
                       id int8 NOT NULL,
                       email varchar(255),
                       full_name varchar(50) NOT NULL,
                       login varchar(30) NOT NULL,
                       password varchar(30) NOT NULL,
                       phone varchar(15),
                       role role NOT NULL
)
;

-- ----------------------------
-- Primary Key structure for table tasks
-- ----------------------------
ALTER TABLE tasks ADD CONSTRAINT tasks_pkey PRIMARY KEY (id);
ALTER TABLE tasks ADD CONSTRAINT name_author_unq UNIQUE (name, author_id);

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE users ADD CONSTRAINT users_pkey PRIMARY KEY (id);

-- ----------------------------
-- Foreign Keys structure for table tasks
-- ----------------------------
ALTER TABLE tasks ADD CONSTRAINT fk_author FOREIGN KEY (author_id) REFERENCES users (id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE tasks ADD CONSTRAINT fk_executor FOREIGN KEY (executor_id) REFERENCES users (id) ON DELETE NO ACTION ON UPDATE NO ACTION;